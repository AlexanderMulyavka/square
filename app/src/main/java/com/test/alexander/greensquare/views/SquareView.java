package com.test.alexander.greensquare.views;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;


public class SquareView extends View {
    private float mClockWise;
    public static final float CLOCK_WISE_FORWARD = 90.0f;
    public static final float CLOCK_WISE_REVERSE = -180.f;

    private int mContentWidth;
    private int mContentHeight;

    public SquareView(Context context) {
        super(context);
        init(null, 0);
    }

    public SquareView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs, 0);
    }

    public SquareView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs, defStyle);
    }

    private void init(AttributeSet attrs, int defStyle) {
        mClockWise = CLOCK_WISE_FORWARD;
    }

    public void setClockWise(float clockWise){
        mClockWise = clockWise;
    }

    public float getClockWise(){
        return mClockWise;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
   }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int paddingLeft = getPaddingLeft();
        int paddingTop = getPaddingTop();
        int paddingRight = getPaddingRight();
        int paddingBottom = getPaddingBottom();

        mContentWidth = MeasureSpec.getSize(widthMeasureSpec) - paddingLeft - paddingRight;
        mContentHeight = MeasureSpec.getSize(heightMeasureSpec) - paddingTop - paddingBottom;

        this.setMeasuredDimension(mContentWidth, mContentHeight);
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }
    @Override
    public boolean onTouchEvent(MotionEvent event){
        if (event.getAction() == MotionEvent.ACTION_DOWN){
            rotateSquare();
            mClockWise = (mClockWise == CLOCK_WISE_FORWARD) ? CLOCK_WISE_REVERSE : CLOCK_WISE_FORWARD;
            return true;
        }
        return false;
    }

    public void rotateSquare(){
        Animation an = new RotateAnimation(0.0f, mClockWise, (mContentWidth)/2, (mContentHeight)/2);

        an.setDuration(1000);
        an.setRepeatCount(0);
        an.setRepeatMode(Animation.RESTART);
        an.setFillAfter(true);

        this.startAnimation(an);
    }
}
