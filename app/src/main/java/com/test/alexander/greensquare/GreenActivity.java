package com.test.alexander.greensquare;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import com.test.alexander.greensquare.views.SquareView;

public class GreenActivity extends AppCompatActivity {

    private SquareView mGreenSquare;
    private String mDirection = "CLOCK_WISE_DIRECTION";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_green);

        mGreenSquare = (SquareView) findViewById(R.id.green_square);
        mGreenSquare.setBackgroundColor(Color.GREEN);
        if (savedInstanceState != null){
            mGreenSquare.setClockWise(savedInstanceState.getFloat(mDirection, SquareView.CLOCK_WISE_FORWARD));
        }
    }

    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putFloat(mDirection, mGreenSquare.getClockWise());

    }

}
